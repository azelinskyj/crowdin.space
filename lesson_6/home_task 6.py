import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_boston
import sklearn.processing as pr


class Linear_Regression():
    def __init__(self, max_iter=1000000, alpha=0.001, eps=1e-10):
        self.max_iter = max_iter
        self.alpha = alpha
        self.eps = eps

    def h(self, b, w, X):
        return b + X @ w.T

    def J(self, h, y):
        return 1 / (2 * self.m) * np.sum((h - y) ** 2)

    def J_derivative(self, params, X, y):
        b, w = params
        h_val = self.h(b, w, X)
        dJ_b = (1 / self.m) * np.sum((h_val - y).T)
        dJ_w = (1 / self.m) * np.sum((h_val - y).T @ X)
        return dJ_b, dJ_w

    def fit(self, X, y):
        self.m, self.n = X.shape
        y = y.reshape(self.m, 1)
        b = 0
        w = np.zeros(self.n).reshape(1, -1)
        params = (b, w)
        self.J_hist = [-1]
        continue_iter = True
        iter_number = 0
        while continue_iter:

            dJ_b, dJ_w = self.J_derivative(params, X, y)
            b = b - self.alpha * dJ_b
            w = w - self.alpha * dJ_w
            params = (b, w)

            self.J_hist.append(self.J(self.h(*params, X), y))
            if self.max_iter and iter_number > self.max_iter:
                continue_iter = False
            elif np.abs(self.J_hist[iter_number - 1] - self.J_hist[iter_number]) < self.eps:
                continue_iter = False
            iter_number += 1
        print(iter_number)
        self.intercept_, self.coef_ = params
        return True

    def draw_cost_changes(self):
        J_hist = self.J_hist[1:]
        plt.figure()
        plt.scatter(np.arange(0, len(J_hist)), J_hist, s=20, marker='.', c='b')
        plt.xlabel('Iterations')
        plt.ylabel('Cost function J value')
        title_str = 'Complited: {}, alpha ={}, max_iter={}, eps={}'.format(len(self.J_hist) - 2, self.alpha,
                                                                           self.max_iter, self.eps)

        plt.title(title_str)

    def draw_scatter(self, X_train, y_train):
        plt.figure()
        plt.scatter(X_train, y_train)
        x_line = np.array([np.min(X_train), np.max(X_train)])
        z_line = self.predict(x_line.reshape(-1, 1))
        plt.plot(x_line, z_line, '-', c='red')
        plt.show()

    def predict(self, X):
        return self.h(self.intercept_, self.coef_, X)

    def score(self, X_test, y_test):
        z = self.predict(X_test)
        from sklearn.metrics.scorer import r2_score
        return r2_score(y_test, z)


if __name__ == '__main__':
    X, y = load_boston(return_X_y=True)
    X = X[:, 5:8]
    X = pr.StandardScaler().fit_transform(X)
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=2018)
    print('X_train.shape= ', X_train.shape)
    print('y_train.shape= ', y_train.shape)
    lin_reg = Linear_Regression(alpha=0.0001, eps=1e-8)
    lin_reg.fit(X_train, y_train)
    lin_reg.draw_cost_changes()
    #lin_reg.draw_scatter(X_train, y_train)
    print('Train: R2 Score =', lin_reg.score(X_train, y_train))
    print('Test: R2 Score =', lin_reg.score(X_test, y_test))
