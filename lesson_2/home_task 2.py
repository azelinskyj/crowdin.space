import pandas as pd
import re
import numpy as np

values_to_convert = {'Republic of Korea': 'South Korea', 'United States of America20': 'United States',
                     'United Kingdom of Great Britain and Northern Ireland19': 'United Kingdom',
                     'China2': 'China', 'Iran, Islamic Rep.': 'Iran',
                     'Bolivia (Plurinational State of)': 'Bolivia', 'Switzerland17': 'Switzerland',
                     'Korea, Rep.': 'South Korea', 'Iran (Islamic Republic of)': 'Iran',
                     'Hong Kong SAR, China': 'Hong Kong', 'Japan10': 'Japan', 'France6': 'France',
                     'Australia1': 'Australia', 'Spain16': 'Spain', 'Italy9': 'Italy'}

column_list = ['Rank', 'Documents', 'Citable documents', 'Citations', 'Self-citations', 'Citations per document',
               'H index', 'Energy Supply', 'Energy Supply per Capita', '% Renewable', '2006', '2007', '2008', '2009',
               '2010', '2011', '2012', '2013', '2014', '2015']

Continents = {'China': 'Asia', 'United States': 'North America', 'Japan':'Asia', 'United Kingdom': 'Europe',
                 'Russian Federation': 'Europe', 'Canada': 'North America', 'Germany': 'Europe', 'India': 'Asia',
                 'France': 'Europe', 'South Korea': 'Asia', 'Italy': 'Europe', 'Spain': 'Europe', 'Iran': 'Asia',
                 'Australia': 'Australia', 'Brazil': 'South America'}

class Data(object):
    def __init__(self, energy, gdp, scimEn):
        self.energy = pd.read_excel(io=energy, sheet_name='Energy', header=16, skiprows=[17],
                                    usecols=[2, 3, 4, 5], skipfooter=38)
        self.energy_editor()

        self.gdp = pd.read_csv(filepath_or_buffer=gdp, header=2, usecols=None)
        self.gdp_editor()
        self.scimEn = pd.read_excel(io=scimEn, sheet_name='Sheet1', nrows=15)

    def energy_editor(self):
        self.energy = self.energy.reset_index()
        self.energy = self.energy.rename(columns={'index': 'Country',
                                                  'Renewable Electricity Production': '% Renewable',
                                                  'Energy Supply per capita': 'Energy Supply per Capita'})
        self.energy['Country'] = self.energy['Country'].apply(self.convert_values)
        self.energy['Energy Supply'] = self.energy['Energy Supply'].apply(self.convert_values)
        self.energy['Energy Supply per Capita'] = self.energy['Energy Supply per Capita'].apply(self.replace_dots)

    def gdp_editor(self):
        self.gdp = self.gdp.rename(columns={'Country Name': 'Country'})
        self.gdp['Country'] = self.gdp['Country'].apply(self.convert_values)

    @staticmethod
    def replace_dots(value):
        if value == '...':
            return None
        return value

    @staticmethod
    def convert_values(value):
        if value in values_to_convert:
            value = values_to_convert[value]
        elif isinstance(value, int):
            value *= 1000000
        elif value == '...':
            value = None
        return value

    @staticmethod
    def check_cols(d_frame):
        for column in d_frame.columns:
            if column not in column_list:
                d_frame = d_frame.drop(column, axis=1)
        return d_frame


class Tasks(Data):
    def __init__(self, energy, gdp, ScimEn):
        super().__init__(energy, gdp, ScimEn)
        self.data = self.answer_1()

    def answer_1(self):
        data = self.scimEn.merge(self.energy, how='left', left_on='Country', right_on='Country')
        data = data.merge(self.gdp, how='left', left_on='Country', right_on='Country')
        data = self.check_cols(data)
        for index in data.index:
            data = data.rename(index={index: self.scimEn.loc[index, 'Country']})
        return data

    def answer_2(self):
        res = [[], []]
        for index in self.data.index:
            avg = 0
            for column in self.data.columns:
                if re.match(r'20\d\d', column) is not None:
                    avg += float(self.data.loc[index, column])
            res[0].append(avg / (2015 - 2006))
            res[1].append(index)
        avgGDP = pd.Series(res[0], res[1]).sort_values(ascending=False)
        return avgGDP

    def answer_3(self):
        country = self.answer_2().index[6 - 1]
        return int(self.data.loc[country, '2015']) - int(self.data.loc[country, '2006'])

    def answer_4(self):
        self.data['Ratio'] = pd.Series()
        max_value = [0, '']
        for index in self.data.index:
            ratio = float(self.data.loc[index, 'Self-citations']) / float(self.data.loc[index, 'Citations'])
            self.data.loc[index, 'Ratio'] = ratio
            if ratio > max_value[0]:
                max_value[0], max_value[1] = ratio, index
        return max_value[0], max_value[1]

    def answer_5(self):
        self.data['Population'] = pd.Series()
        all_population = []
        drop_df = self.data.dropna(subset=['Energy Supply', 'Energy Supply per Capita'])
        for index in drop_df.index:
            population = self.data.loc[index, 'Energy Supply'] / self.data.loc[index, 'Energy Supply per Capita']
            self.data.loc[index, 'Population'] = population
            all_population.append(population)
        self.data.to_excel('data.xls')
        return sorted(all_population)[2]

    def answer_6(self):
        self.data['Citations per Capita'] = pd.Series()
        for index in self.data.index:
            self.data.loc[index, 'Citations per Capita'] = self.data.loc[index, 'H index'] *\
                                                           self.data.loc[index, 'Citations per document']
        corr_data = self.data[['Citations per Capita', 'Energy Supply per Capita']]
        return corr_data.corr(method='pearson').loc['Citations per Capita', 'Energy Supply per Capita']

    def answer_7(self):
        std = {'Asia': [], 'Australia': [], 'Europe': [], 'North America': [], 'South America': []}
        df = pd.DataFrame(index=['Asia', 'Australia', 'Europe', 'North America', 'South America'],
                          columns=['size', 'sum', 'mean', 'std'], data={'size': 0, 'sum': 0})
        for index in self.data.index:
            df.loc[Continents[index], 'size'] = df.loc[Continents[index], 'size'] + 1
            df.loc[Continents[index], 'sum'] = df.loc[Continents[index], 'sum'] + self.data.loc[index, 'Population']
            std[Continents[index]].append(self.data.loc[index, 'Population'])
        for index in df.index:
            if np.std(std[index]) != 0:
                df.loc[index, 'std'] = np.std(std[index])
            df.loc[index, 'mean'] = df.loc[index, 'sum'] / df.loc[index, 'size']
        return df


if __name__ == '__main__':
    Obj = Tasks('Energy Indicators.xls', 'world_blank.csv', 'scimagojr.xlsx')
    Obj.answer_3()
    Obj.answer_4()
    Obj.answer_5()
    Obj.answer_6()
    Obj.answer_7()
